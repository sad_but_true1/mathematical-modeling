import numpy as np
from scipy.constants import G, g
from astropy.constants import M_earth, R_earth
from astropy.units.si import sday

omega_earth = 2 * np.pi / sday.si.scale
M_e = M_earth.value
mu = G * M_e
R = R_earth.value
A = 4.15196 * 10 ** 5 * 10 ** 6
T_gamma = 31556925
epsilon = 2.634 * 10 ** 10 * (10 ** 15)

Nk = 24 * 60 / 95
k = 1
delta = 0

T = (2 * np.pi * k - delta) / Nk * ((omega_earth - 2 * np.pi / T_gamma) ** (-1))
r = (T * np.sqrt(mu) / (2 * np.pi)) ** (2. / 3.)
i = np.arccos(- 2 * np.pi * np.sqrt(mu) / (T_gamma * epsilon) * (r) ** (7 / 2))
print("Task 1")
print("Period = {}".format(T / 60))
print("Height = {}".format((r - R) / 1000))
print("Angle = {}".format(np.rad2deg(i)))


def radius(r_0: float, k: float, Nk: float, i: float, delta: float, eps: float) -> float:
    a = 2 * np.pi * Nk * omega_earth / np.sqrt(mu)
    b = A * Nk * np.cos(i)
    c1 = 3. * a / (8. * np.sqrt(r_0)) - 3. * b / (r_0 ** 4)
    c2 = 3. / 2. * a * np.sqrt(r_0) + 2. * b / (r_0 ** 3)
    c3 = a * (r_0 ** (3. / 2.)) - b * (r_0 ** (-2)) + delta - 2 * np.pi * k
    dr1 = (-c2 - np.sqrt(c2 ** 2 - 4 * c1 * c3)) / (2 * c1)
    dr2 = (-c2 + np.sqrt(c2 ** 2 - 4 * c1 * c3)) / (2 * c1)
    r_1 = max(r_0 + dr1, r_0 + dr2)
    if np.abs(r_0 - r_1) < eps:
        return r_1
    else:
        return radius(r_1, k, Nk, i, delta, eps)


i = np.deg2rad(70)
r_0 = (mu ** (1. / 3.)) * (omega_earth ** (-2. / 3.)) * ((k / Nk) ** (2. / 3.))
r = radius(r_0, k, Nk, i, delta, 10 ** (-8))
T = 2 * np.pi / np.sqrt(mu) * (r ** (3. / 2.))
print("Task 2")
print("Period = {}".format(T / 60))
print("Height = {}".format((r - R) / 1000))

starting_p = 200000
starting_a = 300000
working_r = 514800
u = 317 * g
print("Task 3")
print("ISP = {}".format(u))
m_sat = 1346


def focal(apogee: float, perigee: float) -> float:
    return 2 * perigee * apogee / (perigee + apogee)


def eccentricity(focal: float, perigee: float) -> float:
    return focal / perigee - 1


class Orbit:
    def __init__(self, apogee: float, perigee: float):
        self.a = apogee
        self.p = perigee
        self.f = focal(apogee, perigee)
        self.e = eccentricity(self.f, perigee)


def velocity(orbit: Orbit, theta: float, direction: float = 1.0) -> float:
    return np.sqrt(mu / orbit.f) * (1 + direction * orbit.e * np.cos(theta))


def fuel_mass(m: float, dv1: float, dv2: float, u: float) -> float:
    return m * (np.exp((dv1 + dv2) / u) - 1)


starting = Orbit(R + starting_a, R + starting_p)
middle = Orbit(R + working_r, R + starting_p)
working = Orbit(R + working_r, R + working_r)
v_starting = velocity(starting, 0.0)
v_middle_1 = velocity(middle, 0.0)
dv1 = v_middle_1 - v_starting
v_middle_2 = velocity(middle, 0.0, -1.0)
v_working = velocity(working, 0.0)
dv2 = v_working - v_middle_2
m_f = fuel_mass(m_sat, dv1, dv2, u)
print("The first point\n"
      "Velocity on starting = {}\n"
      "Velocity on middle = {}\n"
      "Impulse 1 = {}\n"
      .format(v_starting / 1000, v_middle_1 / 1000, dv1 / 1000))
print("The second point\n"
      "Velocity on middle = {}\n"
      "Velocity on working = {}\n"
      "Impulse 2 = {}\n"
      .format(v_middle_2 / 1000, v_working / 1000, dv2 / 1000))
print("Total impulse = {}".format((dv1 + dv2) / 1000))
print("Fuel mass = {}".format(m_f))
